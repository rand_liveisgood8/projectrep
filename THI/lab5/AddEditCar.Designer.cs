﻿namespace lab5
{
    partial class AddEditCar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.carClass = new System.Windows.Forms.ComboBox();
            this.carModel = new System.Windows.Forms.TextBox();
            this.carPrice = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.carSeats = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.carCapacity = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // carClass
            // 
            this.carClass.FormattingEnabled = true;
            this.carClass.Location = new System.Drawing.Point(101, 12);
            this.carClass.Name = "carClass";
            this.carClass.Size = new System.Drawing.Size(146, 21);
            this.carClass.TabIndex = 0;
            this.carClass.Text = "PassengerCar";
            this.carClass.SelectedIndexChanged += new System.EventHandler(this.carClass_SelectedIndexChanged);
            // 
            // carModel
            // 
            this.carModel.Location = new System.Drawing.Point(101, 39);
            this.carModel.Name = "carModel";
            this.carModel.Size = new System.Drawing.Size(146, 20);
            this.carModel.TabIndex = 1;
            // 
            // carPrice
            // 
            this.carPrice.Location = new System.Drawing.Point(101, 65);
            this.carPrice.Name = "carPrice";
            this.carPrice.Size = new System.Drawing.Size(146, 20);
            this.carPrice.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(60, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Class:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(56, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Model:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(60, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Price:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(58, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Seats:";
            // 
            // carSeats
            // 
            this.carSeats.Location = new System.Drawing.Point(101, 91);
            this.carSeats.Name = "carSeats";
            this.carSeats.Size = new System.Drawing.Size(146, 20);
            this.carSeats.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Enabled = false;
            this.label5.Location = new System.Drawing.Point(6, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "CarryingCapacity:";
            // 
            // carCapacity
            // 
            this.carCapacity.Enabled = false;
            this.carCapacity.Location = new System.Drawing.Point(101, 117);
            this.carCapacity.Name = "carCapacity";
            this.carCapacity.Size = new System.Drawing.Size(146, 20);
            this.carCapacity.TabIndex = 8;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(39, 169);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(71, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(148, 169);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(71, 23);
            this.button2.TabIndex = 11;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // AddEditCar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(259, 204);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.carCapacity);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.carSeats);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.carPrice);
            this.Controls.Add(this.carModel);
            this.Controls.Add(this.carClass);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "AddEditCar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddEditCar";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox carClass;
        private System.Windows.Forms.TextBox carModel;
        private System.Windows.Forms.TextBox carPrice;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox carSeats;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox carCapacity;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}