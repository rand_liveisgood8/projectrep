﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace lab5
{
    public partial class Main : Form
    {
        private CarPark park = new CarPark();
        public Main() {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) {  //add car
            AddEditCar f2 = new AddEditCar();
            f2.ShowDialog();
            if (f2.Result == DialogResult.OK) {
                carList.Items.Add(f2.Car.Model + " - " + f2.Car.Price.ToString() + "$");
                park.AddCar(f2.Car);
                RefreshPrice();
            }
            f2.Dispose();
        }

        private void button3_Click(object sender, EventArgs e) {  //edit car
            if (carList.SelectedItem != null) {
                AddEditCar f2 = new AddEditCar(park.GetCarList()[carList.SelectedIndex]);
                f2.ShowDialog();
                if (f2.Result == DialogResult.OK) {
                    park.GetCarList()[carList.SelectedIndex] = f2.Car;
                    RefreshPrice();
                }
                f2.Dispose();
            }
            
        }

        private void button2_Click(object sender, EventArgs e) {  //delete car
            if (carList.SelectedItem != null) {
                park.RemoveCar(park.GetCarList()[carList.SelectedIndex]);
                carList.Items.RemoveAt(carList.SelectedIndex);
                RefreshPrice();
            }
        }

        private void RandPark_Click(object sender, EventArgs e) {
            park = CarFactory.CreatePark();
            List<Car> cars = park.GetCarList();

            carList.Items.Clear();
            foreach (Car car in cars) {
                carList.Items.Add(car.Model + " - " + car.Price.ToString() + "$");
            }
            RefreshPrice();
        }

        public void RefreshPrice() {
            priceLabel.Text = park.GetTotalPrice().ToString() + "$";
        }
    }
}
