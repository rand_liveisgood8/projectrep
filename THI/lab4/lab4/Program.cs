﻿using System;

namespace lab4
{
    class Program
    {
        static void Main(string[] args) {
            Console.WriteLine("Console or window? (C / W)");
            if (Console.ReadLine() == "C") {
                CarPark park = CarFactory.CreatePark();

                Printer printer = new Printer();
                printer.Print(park);

                Console.ReadKey();
            } else {
                Main main = new lab4.Main();
                main.ShowDialog();
            }
        }

    }
}