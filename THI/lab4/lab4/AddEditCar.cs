﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab4
{
    public partial class AddEditCar : Form
    {
        private Car _car;
        private DialogResult _result;
        public Car Car
        {
            get { return _car; }
        }
        public DialogResult Result
        {
            get { return _result; }
        }
        public AddEditCar() {
            InitializeComponent();
            carClass.Items.Add(typeof(PassengerCar));
            carClass.Items.Add(typeof(Lorry));
        }

        public AddEditCar(Car car) : this() {
            if (car != null) {
                carClass.Text = car.GetType().ToString();
                carModel.Text = car.Model;
                carPrice.Text = car.Price.ToString();

                if (car.GetType() == typeof(PassengerCar)) {
                    PassengerCar _car = (PassengerCar)car;
                    carSeats.Text = _car.Seats.ToString();

                } else if (car.GetType() == typeof(Lorry)) {
                    Lorry _car = (Lorry)car;
                    carSeats.Text = _car.Seats.ToString();
                    carCapacity.Text = _car.Capacity.ToString();
                }
            }
        }

        private void button1_Click_1(object sender, EventArgs e) {
            if (carClass.Text == "PassengerCar") {
                _car = new PassengerCar(carModel.Text, Convert.ToInt32(carPrice.Text), Convert.ToInt32(carSeats.Text));
            } else {
                _car = new Lorry(carModel.Text, Convert.ToInt32(carPrice.Text), Convert.ToInt32(carSeats.Text), Convert.ToInt32(carCapacity.Text));
            }

            _result = DialogResult.OK;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e) {
            _result = DialogResult.Cancel;
            this.Close();
        }

        private void carClass_SelectedIndexChanged(object sender, EventArgs e) {
            if (carClass.Text == "PassengerCar") {
                carCapacity.Enabled = false;
                label5.Enabled = false;
            } else {
                carCapacity.Enabled = true;
                label5.Enabled = true;
            }
        }
    }
}
