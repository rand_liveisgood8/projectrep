﻿using System;

public class CarFactory
{
    private static string[] cars = { "Toyota", "BMW", "Mercedes", "Lada", "Honda", "Maybach", "Audi", "Ferrari", "Bentley", "Rolls Royse" };
    public static CarPark CreatePark() {
        Random rand = new Random();
        CarPark park = new CarPark();

        int type = rand.Next(0, 1);

        for (int i = 0; i < rand.Next(10, 100); i++) {
            if (type == 0)
                park.AddCar(new PassengerCar(cars[rand.Next(0, cars.Length)], rand.Next(1000, 10000), rand.Next(2, 7)));
            else
                park.AddCar(new Lorry(cars[rand.Next(0, cars.Length)], rand.Next(1000, 10000), rand.Next(4, 10), rand.Next(1000, 5000)));
        }
        return park;
    }
}

