﻿public class Car
{
    protected int _price;
    protected string _model;

    public virtual int Price
    {
        get { return _price; }
        set { _price = value; }
    }

    public string Model
    {
        get { return _model; }
        set { _model = value; }
    }
}

public class PassengerCar : Car
{
    protected int _seats;

    public int Seats
    {
        get { return _seats; }
        set { _seats = value; }
    }

    public override int Price
    {
        set { _price = value + 100 * _seats; }
    }

    public PassengerCar(string model, int price, int seats) {
        Model = model;
        Seats = seats;
        Price = price;
    }
}

public class Lorry : PassengerCar
{
    private int _carryingCapacity;

    public int Capacity
    {
        get { return _carryingCapacity; }
        set { _carryingCapacity = value; }
    }

    public override int Price
    {
        set { _price = value + 100 * _seats + 2 * _carryingCapacity; }
    }
    public Lorry(string model, int price, int seats, int cap) : base(model, price, seats) {
        Model = model;
        Seats = seats;
        Capacity = cap;
        Price = price;
    }
}