﻿using System;
using System.Collections.Generic;


public class CarPark
{
    private List<Car> cars = new List<Car>();

    public void AddCar(Car car) {
        cars.Add(car);
    }

    public void RemoveCar(Car car) {
        cars.Remove(car);
    }

    public int GetTotalPrice() {
        int total_price = 0;
        foreach (Car car in cars) {
            total_price += car.Price;
        }
        return total_price;
    }

    public string[] GetCarListString() {
        string[] r_car = new string[cars.Count];
        for (int i = 0; i < cars.Count; i++) {
            r_car[i] = cars[i].Model + ", " + cars[i].Price.ToString() + "$";
        }
        return r_car;
    }

    public List<Car> GetCarList() {
        return cars;
    }
}

public class Printer
{
    public void Print(CarPark park) {
        List<Car> cars = park.GetCarList();
        for (int i = 0; i < cars.Count; i++) {
            Console.WriteLine((i + 1) + ". " + cars[i].Model + ", " + cars[i].Price.ToString() + "$");
        }
        Console.WriteLine("Total price = " + park.GetTotalPrice() + "$");
    }
}

