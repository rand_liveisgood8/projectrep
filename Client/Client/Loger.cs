﻿using System;
using System.IO;

public static class Loger
{
    private static string _fName;
    public static string fName
    {
        get { return _fName; }
        set { _fName = $"clientLog_{DateTime.Now.ToShortDateString()}.txt"; }
    }

    public static void Write(string str) {
        using (StreamWriter stream = File.AppendText(_fName)) {
            stream.WriteLine(DateTime.Now.ToLongTimeString() + " -> " + str);
        }
    }
}

