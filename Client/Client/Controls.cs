﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;

public static class OControls
{
    public static List<AbstactControl> controls { get; set; } = new List<AbstactControl>();
    private static Graphics _g;
    private static Form _f;

    public static Form f
    {
        get { return _f; }
        set { _f = value; }
    }
    public static Graphics g
    {
        get { return _g; }
        set { _g = value; }
    }

    public static void Initialize(Form _f) {
        f = _f;
        g = f.CreateGraphics();
    }
    public static void ChangeSize(int w, int h) {
        f.Size = new Size(w, h);
        g = f.CreateGraphics();
    }
    public static void Clear()
    {
        g.Clear(Color.White);
    }
    public static void ClearControls() {
        controls.Clear();
    }
    public static void CheckClick(Point p) {
        foreach (AbstactControl cur in controls) {
            if (cur.Click(p))
                break;
        }
    }

}

public class AbstactControl
{
    protected Point _p;
    protected int _w;
    protected int _h;

    public Point p
    {
        get { return _p; }
        set { _p = value; }
    }
    public int w
    {
        get { return _w; }
        set { _w = value; }
    }
    public int h
    {
        get { return _h; }
        set { _h = value; }
    }

    public AbstactControl(Point p, int w, int h) {
        _p = p;
        _w = w;
        _h = h;

		using (Pen pen = new Pen(Color.Black)) {
            OControls.g.DrawRectangle(pen, p.X, p.Y, w, h);
        }
    }

    public virtual bool Click(Point p) {
        if ( (p.X > _p.X) && (p.X < _p.X + _w) && (p.Y > _p.Y) && (p.Y < _p.Y + _h) ) {
            return true;
        }
        return false;
    }
}

public class Label
{
    private string _text;

    public string text
    {
        get { return _text; }
        set { _text = value; }
    }

    public Label(Point p, string text) {
        this.text = text;
        OControls.g.DrawString(text, new Font("Arial", 12, FontStyle.Bold), Brushes.Red, p);
    }
}

public class Btn : AbstactControl
{
    public delegate void ClickHandler();
    public event ClickHandler Clicked; 

    private string _action;
    private Label _lb;

    public Label lb
    {
        get { return _lb; }
        set { _lb = value; }
    }

    public string Action
    {
        get { return _action;  }
        set { _action = value; }
    }

    public Btn(Point p, int w, int h, string action) : base(p, w, h) {
        _action = action;
        lb = new Label(p, _action);
    }
    public override bool Click(Point p) {
        if (base.Click(p)) {
            Clicked();
            return true;
        }
        return false;
    }
}
public class TextBox : AbstactControl
{
    //События ввода
    public delegate void InputHandler(string value);
    public event InputHandler KeyPress;

    private int text_curX;
    private int limit;
    private string _text;
    private bool _focused;

    public bool focused
    {
        get { return _focused; }
        set { _focused = value; }
    }
    public string text
    {
        get { return _text; }
        set { _text = value; }
    }

    public TextBox(Point p, int w, int h, int limit, bool focused = true) : base(p, w, h) {
        text_curX = p.X;
        this.limit = limit;
        this.focused = focused;
        text = string.Empty;
    }
    public override bool Click(Point p) {
        if (base.Click(p)) {
            focused = true;
            return true;
        }
        return false;
    }
    public void Input(char key) {
        if (key == (char)Keys.Back) {
            //Стереть весь текст
            OControls.g.FillRectangle(Brushes.White, new Rectangle(p, new Size(w, h)));
            text = string.Empty;
            text_curX = p.X;
            return;
        }
        else if (key == (char)Keys.Enter) {
            //Вызов события
            KeyPress(text);
            return;
        }
        using (Pen pen = new Pen(Color.Black, 2)) {
            if (text.Length < limit) {
                OControls.g.DrawString(key.ToString(), new Font("Arial", 12, FontStyle.Bold), Brushes.Red, new Point(text_curX, p.Y));
                text += key.ToString();
                text_curX += 10;
            }
            else {
                MessageBox.Show("Вы достигли лимита символов!");
            }
        }
    }
}
