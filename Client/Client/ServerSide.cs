﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Client;
public static class Server
{
    private static NetworkStream stream;
    private static TcpClient main;
    private static ManualResetEvent mrse = new ManualResetEvent(true);
    private static bool stop = false;

    public static bool Stop
    {
        get { return stop; }
        set { stop = value; }
    }
    public static string Connect(string name) {
        try {
            main = new TcpClient();
            main.Connect(Parms.host, Parms.port);
            stream = main.GetStream();
            Loger.Write("Подключение к серверу установлено");

            int res = Auth(name);
            if (res == 1)
                return "true";
            else if (res == 0)
                return "false";
            else
                return "Непридвиденная ошибка авторизации!";
        }
        catch (Exception e) {
            Loger.Write($"{e.Message} Connect(); ");
            return e.Message;
        }
    }

    public static void Listen() {
        while (!stop) {
            mrse.WaitOne();
            Cmd cmd = Get();
            if (cmd.name == "request") {
                DialogResult res = MessageBox.Show($"Вам пришел запрос на игру от {cmd.text}, вы хотите сыграть?", "Игра",
                    MessageBoxButtons.YesNo);
                if (res == DialogResult.Yes) {
                    Send("game:accept");
                    Game game = new Game(new System.Drawing.Point(10, 30), new System.Drawing.Point(300, 30), cmd.text);
                }
                else if (res == DialogResult.No) {
                    Send("game:reject");
                }
            }
        }
        CloseConnection();
    }

    //Логика сервера
    public static void Resume()
    {
        mrse.Set();
    }
    public static void Pause()
    {
        mrse.Reset();
    }
    public static bool Send(string send) {
        try {
            if (GetState()) {
                Loger.Write($"Отправили на сервер: {send}");
                byte[] data = Encoding.UTF8.GetBytes(send);
                stream.Write(data, 0, data.Length);
                return true;
            }
            else {
                MessageBox.Show("Сервер не отвечает.");
                Application.Exit();
            }
        }
        catch (Exception e) {
            Loger.Write($"SEND: {e.Message}");
            MessageBox.Show("Ошибка при отправке данных на сервер!");
        }
        return false;
    }
    public static Cmd Get(bool wait = false) {
        Cmd cmd;
        cmd.name = null;
        cmd.text = null;
        try {
            if (GetState()) {
                byte[] data = new byte[33554432]; // буфер для получаемых данных
                if (stream.DataAvailable) {
                    int bytes = stream.Read(data, 0, data.Length);
                    string message = Encoding.UTF8.GetString(data, 0, bytes);

                    cmd.name = message.Split(':')[0];
                    cmd.text = message.Split(':')[1];
                }
                else if (wait) {
                    int bytes = stream.Read(data, 0, data.Length);
                    string message = Encoding.UTF8.GetString(data, 0, bytes);

                    cmd.name = message.Split(':')[0];
                    cmd.text = message.Split(':')[1];
                }
            }
            else {
                MessageBox.Show("Сервер не отвечает.");
                Application.Exit();
            }
        }
        catch (Exception e) {
            stop = true;
            Loger.Write($"GET: {e.Message}");
            MessageBox.Show("Ошибка при приеме данных с сервера!");
        }
        return cmd;
    }
    private static bool GetState()
    {
        if (main.Client.Poll(0, SelectMode.SelectRead)) {
            byte[] buff = new byte[1];
            if (main.Client.Receive(buff, SocketFlags.Peek) == 0) {
                // Client disconnected
                return false;
            }
        }
        return true;
    }
    public static void StopServer()
    {
        Server.Stop = true;
        Server.Resume();
    }
    public static void CloseConnection()
    {
        if (stream != null)
            stream.Close();
        if (main != null)
            main.Close();
    }

    //Логика игры
    public static int GameRequest(string name)
    {
        Pause();

        if (Send("request:" + name)) {
            Cmd get = Get(true);
            Resume();

            if (get.text == "accept") {
                MessageBox.Show("Игра начинается!");
                return 1;
            }
            else if (get.text == "reject") {
                MessageBox.Show("Игра не принята!");
                return 0;
            }
            else if (get.text == "offline") {
                MessageBox.Show("Игрок не в сети!");
                return 2;
            }
        }
        return -1;
    }

    public static int Auth(string value)
    {
        Loger.Write("Попытка авторизации...");
        if (Send($"auth:{value}")) {
            Cmd get = Get(true);
            if (get.text == "success")
                return 1;
            else if (get.text == "error")
                return 0;
        }
        return -1;
    }
    public static int Shot(int i, int j)
    {
        Parms.STEP = false;
        int hit = 0;
        if (Send($"shot:{i.ToString()}.{j.ToString()}")) {
            Cmd cmd = Get(true);
            if (cmd.name == "shot" && cmd.text == "true") {
                hit = 1;
            }
            else if (cmd.name == "game-win") {
                return 2;
            }
            else if (cmd.name == "game-error") {
                EndGame(cmd.text, true);
                return -1;
            }
        }
        WaitShot();
        return hit;
    }

    public static async void WaitShot()
    {
         await Task.Run(() => {
             string winner = null;
            Cmd cmd = Get(true);

             if (cmd.name == "game-error") {
                 EndGame(cmd.text, true);
                 return;
             }
             else if (cmd.name == "game-lose") {
                 string[] p = cmd.text.Split('=');
                 cmd.text = p[0];
                 winner = p[1];
             }

            int i = Convert.ToInt32(cmd.text.Split('.')[0]);
            int j = Convert.ToInt32(cmd.text.Split('.')[1]);

            if (cmd.name == "hit") {
                Game.MyField.FBox[i, j].ShotIt(System.Drawing.Brushes.Black);
            }
            else if (cmd.name == "miss") {
                Game.MyField.FBox[i, j].ShotIt(System.Drawing.Brushes.Gold);
            }
            else if (cmd.name == "game-lose") {
                Game.MyField.FBox[i, j].ShotIt(System.Drawing.Brushes.Black);
                EndGame(winner);
            }
            Parms.STEP = true;
        });    
    }

    public static void EndGame(string winner, bool error = false)
    {
        if (error)
            MessageBox.Show($"Ошибка в процессе игры\n{winner}");
        else
            MessageBox.Show($"Игра окончена, победил {winner}");

        Draw.DrawMenu();
        Server.Resume();
    }

    public static void SendField()
    {
        string f = "field:";
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (Game.MyField.FBox[i,j].Ship) {
                    f += $"{i}.{j},";
                }
            }
        }
        f = f.Remove(f.Length - 1);
        Send(f);

        if (!Parms.STEP) {
            WaitShot();
        }
    }
}

public struct Cmd
{
    public string name;
    public string text;
}

