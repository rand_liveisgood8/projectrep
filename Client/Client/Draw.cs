﻿using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace Client
{
    public class Draw
    {
        //Отрисовка форм
        public static void DrawWelcome()
        {
            OControls.ClearControls();
            TextBox name = new TextBox(new Point(20, 100), 350, 20, 20);
            name.KeyPress += (value) => {
                string res = Server.Connect(value);
                if (res == "true") {
                    Thread server = new Thread(new ThreadStart(Server.Listen));
                    server.Start();

                    Parms.NAME = value;
                    DrawMenu();
                }
                else if (res == "false") {
                    MessageBox.Show("Пользователь с таким логином уже подключен к серверу, выберите другой логин.");
                }
                else {
                    MessageBox.Show($"Ошибка подключения к серверу:\n{res}");
                }
            };

            OControls.controls.Add(name);
            new Label(new Point(20, 40), "Привет, для начала игры введи свое имя: ");
        }
        public static void DrawMenu()
        {
            OControls.ChangeSize(410, 200);
            OControls.Clear();
            OControls.ClearControls();

            Label name = new Label(new Point(5, 5), "Привет, " + Parms.NAME);

            Btn play = new Btn(new Point(40, 40), 100, 80, "Играть");
            play.Clicked += DrawSelectEnemy;

            OControls.controls.Add(play);
            OControls.controls.Add(new Btn(new Point(150, 40), 100, 80, "Настройки"));
            OControls.controls.Add(new Btn(new Point(260, 40), 100, 80, "TestConnect"));
            Parms.CURR_STATE = Parms.MENU;
        }
        public static void DrawSelectEnemy()
        {
            OControls.Clear();
            OControls.ClearControls();

            new Label(new Point(5, 5), "Введите имя противника: ");

            TextBox enemy = new TextBox(new Point(5, 30), 350, 20, 20);
            enemy.KeyPress += (value) => {
                if (Server.GameRequest(value) == 1) {
                    Parms.STEP = true;
                    Game game = new Game(new Point(10, 30), new Point(300, 30), value);
                }
            };

            Btn back = new Btn(new Point(10, 80), 50, 30, "back");
            back.Clicked += () => {
                DrawMenu();
            };

            OControls.controls.Add(back);
            OControls.controls.Add(enemy);
            Parms.CURR_STATE = Parms.SELECT_PLAYER;
        }
    }
}
