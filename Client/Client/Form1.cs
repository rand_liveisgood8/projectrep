﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Client
{
    public partial class Form1 : Form
    {
        Ship ship = null;
        public Form1() {
            InitializeComponent();
            OControls.Initialize(this);
            Loger.fName = "client_log";
            CheckForIllegalCrossThreadCalls = false;
        }

        private void Form1_Shown(object sender, EventArgs e) {
            Draw.DrawWelcome();
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e) {
            if (Parms.CURR_STATE == Parms.IN_GAME)
                Game.VsField.Click(e.X, e.Y);
            else
                OControls.CheckClick(new Point(e.X, e.Y));
        }
        private void Form1_KeyPress(object sender, KeyPressEventArgs e) {
            if (Parms.CURR_STATE != Parms.SHIP_ARRANGEMENT) {
                TextBox tBox = null;
                foreach (AbstactControl text in OControls.controls)  {
                    if (text.GetType() == typeof(TextBox)) {
                        tBox = (TextBox)text;
                        if (tBox.focused) {
                            tBox.Input(e.KeyChar);
                            break;
                        }
                    }                
                }
            }
            else if (Parms.CURR_STATE == Parms.SHIP_ARRANGEMENT) {           
                if (e.KeyChar == Convert.ToChar(Keys.Space)) {
                    if (ship != null) {
                        Game.MyField.AddShip(ship);
                        ship = new Ship(0, 0, 0);
                    }                  
                    else
                        ship = new Ship(0, 0, 0);
                } else if (e.KeyChar == 81 || e.KeyChar == 113) { //Q
                    if (ship.Direction == 1)
                        ship.Direction = 0;
                    else
                        ship.Direction = 1;
                }
                else if (e.KeyChar == 87 || e.KeyChar == 119) {  //W
                    ship.i--;
                } else if (e.KeyChar == 83 || e.KeyChar == 115) {  //S
                    ship.i++;
                } else if (e.KeyChar == 64 || e.KeyChar == 97) {   //A
                    ship.j--;
                } else if (e.KeyChar == 68 || e.KeyChar == 100) {  //D
                    ship.j++;
                }
                
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e) {
            Server.StopServer();
        }
    }
}
