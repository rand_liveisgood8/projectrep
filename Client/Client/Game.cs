﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

public class Game
{
    private static bool step;
    private static Field myField;
    private static VSField vsField;
    private string enemy;

    public static VSField VsField
    {
        get { return vsField; }
        set { vsField = value; }
    }


    public static Field MyField
    {
        get { return myField; }
        set { myField = value; }
    }


    public static bool Step
    {
        get { return step; }
        set { step = value; }
    }

    public Game(Point myF, Point vsF, string enemy) {
        OControls.ClearControls();
        OControls.ChangeSize(700, 400);
        OControls.Clear();

        Server.Pause();

        myField = new Field(myF);
        vsField = new VSField(vsF);
        new Label(new Point(0, 0), $"Ваш противник: {enemy}");
        this.enemy = enemy;

        Parms.CURR_STATE = Parms.SHIP_ARRANGEMENT;  
    }
}
public class Field
{
    private List<Ship> ships = new List<Ship>();
    public Box[,] FBox = new Box[10, 10];
    protected Point p;
    public Field(Point p) {
        this.p = p;

        //Отрисовка поля
        int x_old = p.X;
        int dX = Parms.BOX_SIZE;
        int dY = Parms.BOX_SIZE;

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                FBox[i, j] = new Box(p, i, j);
                p.X += dX;
            }
            p.Y += dY;
            p.X = x_old;
        }
    }
    public void Refresh() {
        foreach (Box b in FBox) {
            if (b.Ship) {
                b.PaintIt();
            }
        }
    }

    public void AddShip(Ship ship)
    {
        ship.Set();
        ships.Add(ship);
    }
    public void RemoveShip(Ship ship)
    {
        ships.Remove(ship);
    }
    public List<Ship> GetShips()
    {
        return ships;
    }
    
    //Получить кол-во кораблей заданого размера
    public int GetShipsCount(int size)
    {
        return ships.FindAll(ship => ship.Size == size).Count;
    }
}
public class VSField : Field
{
    public VSField(Point p) : base(p) {
    }
    public void Click(int _x, int _y) {
        if (!Parms.STEP) {
            MessageBox.Show("Не ваш ход!");
            return;
        }
        foreach (Box b in FBox)
            if ((_x > b.P.X) && (_x < b.P.X + Parms.BOX_SIZE) && (_y > b.P.Y) && (_y < b.P.Y + Parms.BOX_SIZE)) {
                int res_shot = Server.Shot(b.i, b.j);
                if (res_shot == 1) 
                    b.ShotIt(Brushes.Red);
                else if (res_shot == 0)
                    b.ShotIt(Brushes.Aqua);
                else if (res_shot == 2) {
                    b.ShotIt(Brushes.Red);
                    Server.EndGame(Parms.NAME);
                }
                break;
            }
    }
}
public class Box
{
    private Point p;
    private int _i;
    private int _j;
    private bool _ship = false;
    private bool _shooted = false;

    public bool Shooted
    {
        get { return _shooted = false; }
        set { _shooted = value; }
    }
    public bool Ship
    {
        get { return _ship; }
        set { _ship = value; }
    }
    public int j
    {
        get { return _j; }
        set { _j = value; }
    }
    public int i
    {
        get { return _i; }
        set { _i = value; }
    }
    public Point P
    {
        get { return p; }
        set { p = value; }
    }

    public Box(Point point, int i, int j) {
        p = point;
        _i = i;
        _j = j;

        using (Pen pen = new Pen(Color.Black, 2))
            OControls.g.DrawRectangle(pen, p.X, p.Y, Parms.BOX_SIZE, Parms.BOX_SIZE);
    }
    public void PaintIt() {
        OControls.g.FillRectangle(Brushes.Red, p.X + 1, p.Y + 1, Parms.BOX_SIZE - 2, Parms.BOX_SIZE - 2);
    }
    public void ClearIt() {
        OControls.g.FillRectangle(Brushes.White, p.X + 1, p.Y + 1, Parms.BOX_SIZE - 2, Parms.BOX_SIZE - 2);
    }
    public void ShotIt(Brush fill) {
        OControls.g.FillRectangle(fill, p.X + 1, p.Y + 1, Parms.BOX_SIZE - 2, Parms.BOX_SIZE - 2);
    }
}

public class Ship
{
    private int _size;
    private int _direction;
    private int _i;
    private int _j;

    public int j
    {
        get { return _j; }
        set {
            Erase();
            _j = value;
            Draw();
        }
    }
    public int i
    {
        get { return _i; }
        set {
            Erase();
            _i = value;
            Draw();
        }
    }
    public int Direction
    {
        get { return _direction; }
        set {
            Erase();
            _direction = value;
            Draw();
        }
    }
    public int Size
    {
        get { return _size; }
        set { _size = value; }
    }

    public void Move(int i, int j, int direction)
    {
        Erase();
        _i = i;
        _j = j;
        _direction = direction;
        Draw();
    }

    public void ChangeDirection(int direction)
    {
        Erase();
        _direction = direction;
        Draw();
    }
    public Ship(int i, int j, int direction)
    {
        _direction = direction;
        _i = i;
        _j = j;

        if (Game.MyField.GetShipsCount(4) < 1) {
            _size = 4;
        }
        else if (Game.MyField.GetShipsCount(3) < 2) {
            _size = 3;
        }
        else if (Game.MyField.GetShipsCount(2) < 3) {
            _size = 2;
        }
        else if (Game.MyField.GetShipsCount(1) < 4) {
            _size = 1;
        }
        else {
            Parms.CURR_STATE = Parms.IN_GAME;
            Server.SendField();
        }

        Draw();
    }
    public void Set()
    {
        for (int k = 0; k < _size; k++) {
            if (_direction == 0) { //vniz
                Game.MyField.FBox[i + k, j].Ship = true;
            }
            else if (_direction == 1) { //pravo
                Game.MyField.FBox[i + k, j].Ship = true;
            }
        }
    }
    public void Draw()
    {
        for (int k = 0; k < _size; k++) {
            if (_direction == 0) { //vniz
                Game.MyField.FBox[i + k, j].PaintIt();
            }
            else if (_direction == 1) { //pravo
                Game.MyField.FBox[i, j + k].PaintIt();
            }
        }
    }

    public void Erase()
    {
        for (int k = 0; k < _size; k++) {
            if (_direction == 0) {//vniz
                if (!Game.MyField.FBox[i + k, j].Ship)
                    Game.MyField.FBox[i + k, j].ClearIt();
            }
            else if (_direction == 1) { //pravo
                if (!Game.MyField.FBox[i, j + k].Ship)
                    Game.MyField.FBox[i, j + k].ClearIt();
            }
        }
    }
}